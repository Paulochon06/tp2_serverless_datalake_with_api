# TODO : Create the lambda role with aws_iam_role
resource "aws_iam_role" "test_lambda" {
  name = "test_lambda"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF
}
# TODO : Create lambda function with aws_lambda_function
resource "aws_lambda_function" "test_lambda" {
  function_name = var.data_processing_lambda_lambda_name#"function1"
  filename      = "non_empty_lambda_code.zip"#can't use an empty zip
  handler       = "lambda_main_app.lambda_handler"
  runtime       = "python3.7"
  #role          = aws_iam_role.iam_for_lambda.arn
   role          = aws_iam_role.test_lambda.arn
}
# TODO : Create a aws_iam_policy for the logging, this resource policy will be attached to the lambda role
resource "aws_iam_policy" "lambda_logging" {
  name        = "lambda_logging"
  path        = "/"
  description = "IAM policy for logging from a lambda"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF
}
# TODO : Attach the logging policy to the lamda role with aws_iam_role_policy_attachment
resource "aws_iam_role_policy_attachment" "lambda_logs" {
  role       = aws_iam_role.test_lambda.name
  policy_arn = aws_iam_policy.lambda_logging.arn
}
# TODO : Attach the AmazonS3FullAccess policy to the lambda role with aws_iam_role_policy_attachment
#Already defined in the aws_iam_role
# TODO : Allow the lambda to be triggered by a s3 event with aws_lambda_permission
resource "aws_lambda_permission" "allow_lambda" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.function_name
  principal     = "s3.amazonaws.com"
}
