# TODO : Create a s3 bucket with aws_s3_bucket
resource "aws_s3_bucket" "bucket" {
  bucket = var.s3_user_bucket_name#"s3-job-offer-bucket-paul-guillaume-hussein"
  acl    = "private"
  force_destroy = true
}


# TODO : Create 1 nested folder :  job_offers/raw/  |  with  aws_s3_bucket_object
resource "aws_s3_bucket_object" "job_offers" {
  bucket = var.s3_user_bucket_name#"s3-job-offer-bucket-paul-guillaume-hussein"
  key    = "job_offers/raw/"
}
# TODO : Create an event to trigger the lambda when a file is uploaded into s3 with aws_s3_bucket_notification
resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.test_lambda.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.bucket.arn
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.test_lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "job_offers/raw/"
    #filter_suffix       = ".log"
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
